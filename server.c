#include "funcechoServ.h"

int main(int argc, char *argv[]) {
    
    char buf[BUF_SIZ];

    /* Header structures */
    struct ether_header *eh = (struct ether_header *) buf;
    struct iphdr *iph = (struct iphdr *) (buf + sizeof(struct ether_header));
    struct udphdr *udph = (struct udphdr *) (buf + sizeof(struct iphdr) + sizeof(struct ether_header));

    // memset(&if_ip, 0, sizeof(struct ifreq));

    int sockfd;

    /* Open PF_PACKET socket, listening for EtherType ETHER_TYPE */
    if ((sockfd = socket(PF_PACKET, SOCK_RAW, htons(ETHER_TYPE))) == -1) {
        perror("listener: socket"); 
        return -1;
    }

    struct ifreq ifopts;                                /* set promiscuous mode */

    /* Set interface to promiscuous mode - do we need to do this every time? */
    strncpy(ifopts.ifr_name, DEFAULT_IF, IFNAMSIZ-1);
    ioctl(sockfd, SIOCGIFFLAGS, &ifopts);

    ifopts.ifr_flags |= IFF_PROMISC;

    ioctl(sockfd, SIOCSIFFLAGS, &ifopts);
    /* Allow the socket to be reused - incase connection is closed prematurely */
    
    int sockopt;

    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &sockopt, sizeof(sockopt)) == -1) {
        perror("setsockopt");
        close(sockfd);
        exit(EXIT_FAILURE);
    }
    /* Bind to device */
    if (setsockopt(sockfd, SOL_SOCKET, SO_BINDTODEVICE, DEFAULT_IF, IFNAMSIZ-1) == -1)  {
        perror("SO_BINDTODEVICE");
        close(sockfd);
        exit(EXIT_FAILURE);
    }

    unsigned int n = 1;
    char *filtredIP[n];
    /* Фильтруемый IP */ 
    char bufip[BUF_SIZ] = "192.168.1.4";                                        


    for(int i = 0; i < n; i++) {
        filtredIP[i] = malloc(sizeof(char)*strlen(bufip));
        strcpy(filtredIP[i], bufip);
    }

    struct ifreq if_ip;                     /* get ip addr */
    struct sockaddr_storage their_addr;
    ssize_t numbytes;
    char sender[BUF_SIZ];
    uint8_t macserver[] = {0x20, 0x1a, 0x06, 0x6e, 0x6f, 0xe4};

    while(1) {
        printf("listener: Waiting to recvfrom...\n");
        numbytes = recvfrom(sockfd, buf, BUF_SIZ, 0, NULL, NULL);
        printf("listener: got packet %zd bytes\n", numbytes);

        /* Check the packet is for me */
        int check;
        check = checkingMAC(buf, sockfd);

        if(check == 1) {
            struct iphdr *iph = (struct iphdr *)(buf + sizeof(struct ether_header));
            ((struct sockaddr_in *)&their_addr)->sin_addr.s_addr = iph->saddr;
            inet_ntop(AF_INET, &(((struct sockaddr_in*)&their_addr)->sin_addr), sender, sizeof(sender));     
            if (n > 0) {
                for(int i = 0; i < n; i++) {
                    if (strcmp(sender, filtredIP[i]) == 0) {
                        /* Принятие пакетов пришедших на MAC-адрес и фильтруемый IP*/
                        infopacket(buf , numbytes);
                        
                        /* Формирование пакета для отправки обратно клиенту */

                        int socktocl;

                        if ((socktocl = socket(AF_PACKET, SOCK_RAW, IPPROTO_UDP)) == -1) {
                            perror("socket");
                        }

                        int tx_len = 0;
                        char sendbuf[BUF_SIZ];
                        struct ether_header *ehcl = (struct ether_header*)sendbuf;

                        memset(sendbuf, 0, BUF_SIZ);

                        for(int i = 0; i < LENMAC; i++) {
                            ehcl->ether_shost[i] = (uint8_t)eh->ether_dhost[i];
                            ehcl->ether_dhost[i] = (uint8_t)eh->ether_shost[i];
                        }
                        ehcl->ether_type = (uint8_t)eh->ether_type;
                        tx_len += sizeof(struct ether_header);

                        struct iphdr *iphcl = (struct iphdr *) (sendbuf + sizeof(struct ether_header));

                        iphcl->ihl = iph->ihl;
                        iphcl->version = iph->version;
                        iphcl->tos = iph->tos;
                        iphcl->id = iph->id;
                        iphcl->ttl = iph->ttl;
                        iphcl->protocol = iph->protocol;
                        inet_ntop(AF_INET, &(iph->daddr), sender, sizeof(sender));
                        iphcl->saddr = inet_addr(sender);
                        inet_ntop(AF_INET, &(iph->saddr), sender, sizeof(sender));
                        iphcl->daddr = inet_addr(sender);
                        tx_len += sizeof(struct iphdr);

                        struct udphdr *udphcl = (struct udphdr *) (sendbuf + sizeof(struct iphdr) + sizeof(struct ether_header));

                        udphcl->source = udph->dest;
                        udphcl->dest = udph->source;
                        udphcl->check = udph->check;
                        tx_len += sizeof(struct udphdr);

                        for(int i = sizeof(struct ether_header)+sizeof(struct iphdr)+sizeof(struct udphdr); i < numbytes; i++) {
                            sendbuf[tx_len++] = buf[i]; 
                        }

                        udphcl->len = htons(tx_len - sizeof(struct ether_header) - sizeof(struct iphdr));
                        iphcl->tot_len = htons(tx_len - sizeof(struct ether_header));

                        struct sockaddr_ll socket_address;

                        struct ifreq if_idx;
                        
                        memset(&if_idx, 0, sizeof(struct ifreq));
                        strncpy(if_idx.ifr_name, DEFAULT_IF, IFNAMSIZ-1);

                        
                        if (ioctl(sockfd, SIOCGIFINDEX, &if_idx) < 0) {         
                            perror("SIOCGIFINDEX");
                        }

                        socket_address.sll_ifindex = if_idx.ifr_ifindex;

                        socket_address.sll_halen = ETH_ALEN;

                        for(int i = 0; i < LENMAC; i++) {
                            socket_address.sll_addr[i] = (uint8_t)eh->ether_shost[i];
                        }

                        if (sendto(socktocl, sendbuf, tx_len, 0, (struct sockaddr*)&socket_address, sizeof(struct sockaddr_ll)) != -1) {
                            printf("Sended\n");
                        }
                        else {
                            printf("Error: %s\n", strerror(errno));
                        }
                        
                        close(socktocl);
                    }
                }
            }
            else {
                /* Принятие всех пакетов пришедших на MAC-адрес */
                infopacket(buf, sockfd);
            }
        }
        else {
            continue;
        }
    }

    close(sockfd);
    
    return 0;
}